export class Specie {

    constructor(public name: string, public classification: string) {}

}

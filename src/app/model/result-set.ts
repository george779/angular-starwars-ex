import { Planet } from './planet';

export class ResultSet {
    constructor(public count: number, public results: Planet[]) {}

}
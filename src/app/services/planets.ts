import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class Planets {

    constructor(private httpClient: HttpClient) { }

    getItems(itemType: string): Observable<Object> {
        return this.httpClient.get<Object>(
          environment.backendUrl + '/' + itemType + '/');
      }

}

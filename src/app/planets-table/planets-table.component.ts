import { Component, OnInit } from '@angular/core';
import { Planet } from '../model/planet';
import { Planets } from '../services/planets';
import { Person } from '../model/person';
import { Film } from '../model/film';
import { Specie } from '../model/specie';
import { Vehicle } from '../model/vehicle';
import { Starship } from '../model/starship';

@Component({
  selector: 'app-planets-table',
  templateUrl: './planets-table.component.html',
  styleUrls: ['./planets-table.component.css']
})
export class PlanetsTableComponent implements OnInit {

  planets: Planet[];
  people: Person[];
  films: Film[];
  species: Specie[];
  vehicles: Vehicle[];
  starships: Starship[];
  hidePeople = true;
  hidePlanets = true;
  hideFilms = true;
  hideSpecies = true;
  hideVehicles = true;
  hideStarships = true;

  itemType = 'planets';

  constructor(private planetService: Planets) { }

  ngOnInit() {
    this.getPlanets();
  }

  getPlanets(){
    this.getItems('planets');
  }

  getCurrentItemType() {
    this.getItems(this.itemType);
  }

  getItems(itemType: string){
    console.log('getting items: ' + itemType);
    this.planetService.getItems(itemType).subscribe(
      data => {
        console.log('Component Received Itemsfrom Backend!');
        console.log(data);

        if(itemType === 'planets') {
          this.planets = data['results'];
          this.hidePeople = true;
          this.hidePlanets = false;
          this.hideFilms = false;
          this.hideSpecies = false;
          this.hideVehicles = false;
          this.hideStarships = false;
        }
        else if(itemType === 'people') {
          this.people = data['results'];
          this.hidePeople = false;
          this.hidePlanets = true;
          this.hideFilms = false;
          this.hideSpecies = false;
          this.hideVehicles = false;
          this.hideStarships = false;
        }
        else if(itemType === 'films') {
          this.films = data['results'];
          this.hidePeople = false;
          this.hidePlanets = false;
          this.hideFilms = true;
          this.hideSpecies = false;
          this.hideVehicles = false;
          this.hideStarships = false;
        }
        else if(itemType === 'species') {
          this.species = data['results'];
          this.hidePeople = false;
          this.hidePlanets = false;
          this.hideFilms = false;
          this.hideSpecies = true;
          this.hideVehicles = false;
          this.hideStarships = false;
        }
        else if(itemType === 'vehicles') {
          this.vehicles = data['results'];
          this.hidePeople = false;
          this.hidePlanets = false;
          this.hideFilms = false;
          this.hideSpecies = false;
          this.hideVehicles = true;
          this.hideStarships = false;
        }
        else if(itemType === 'starships') {
          this.starships = data['results'];
          this.hidePeople = false;
          this.hidePlanets = false;
          this.hideFilms = false;
          this.hideSpecies = false;
          this.hideVehicles = false;
          this.hideStarships = true;
        }
      },
      error => {
        console.log('An error occurred');
        console.log(error);
      }
    );
  }
}



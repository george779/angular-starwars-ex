import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetsTableFormComponent } from './planets-table-form.component';

describe('PlanetsTableFormComponent', () => {
  let component: PlanetsTableFormComponent;
  let fixture: ComponentFixture<PlanetsTableFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanetsTableFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetsTableFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

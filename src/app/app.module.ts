import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlanetsTableComponent } from './planets-table/planets-table.component';
import { HttpClientModule } from '@angular/common/http';
import { PlanetsTableFormComponent } from './planets-table-form/planets-table-form.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PlanetsTableComponent,
    PlanetsTableFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
